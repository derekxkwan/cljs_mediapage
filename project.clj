(defproject cljs-mediapage "0.0.1"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.229"]
                 [reagent "0.6.0"]
                 [cljs-ajax "0.7.3"]
                 [reagent-forms "0.5.42"]]
  :plugins [[lein-cljsbuild "1.0.6"]
            [lein-figwheel "0.5.7"]]
  :clean-targets ^{:protect false} [:target-path "out" "resources/public/cljs"]
  :cljsbuild {
              :builds [{:id "dev"
                        :source-paths ["src"]
                        :figwheel true
                        :compiler {:main "cljs-mediapage.core"
                                   :asset-path "cljs/out"
                                   :output-to "resources/public/cljs/main.js"
                                   :output-dir "resources/public/cljs/out"}
                        }]
              }
  :figwheel {
             :css-dirs ["resources/public"]
             }
  :repl-options {:color false}
  )
