(ns cljs-mediapage.cleanser)

(defn form-youtube-player-url [url]
  (str "https://www.youtube.com/embed/" (str url)))

(defn form-soundcloud-player-url [url]
  (str "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/" (str url)
       "&amp;color=0066cc&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false" ))

(defn form-bandcamp-link-url [url]
  (str "http://derekxkwan.bandcamp.com/track/" (str url)))

(defn form-bandcamp-player-url [url]
  (str "https://bandcamp.com/EmbeddedPlayer/track=" (str url)
       "/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/"))
  
(defn youtube-player-element [url]
  [:iframe {:width "560" :height "315" :src url :allowfullscreen true :frameborder "0"}])

(defn soundcloud-player-element [url]
  [:iframe {:width "100%" :height "166" :scrolling "no" :frameborder "no" :src url}])

(defn bandcamp-player-element [url]
  [:iframe {:style {:border "0" :width "100%" :height "42px"} :src url :seamless true}])


(defn parse-url [cur-type url]
  (cond
    (= cur-type "YouTube") (form-youtube-player-url url)
    (= cur-type "SoundCloud") (form-soundcloud-player-url url)
    (= cur-type "Bandcamp") (form-bandcamp-player-url url)
    :else url))

(defn parse-player [cur-type url]
  (let [parsed (parse-url url)]
    (cond
      (= cur-type "YouTube") (youtube-player-element parsed)
      (= cur-type "SoundCloud") (soundcloud-player-element parsed)
      (= cur-type "BandCamp") (bandcamp-player-element parsed)
      :else nil)))

