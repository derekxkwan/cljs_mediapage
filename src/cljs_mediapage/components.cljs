(ns cljs-mediapage.components
  (:require [cljs-mediapage.cleanser :as cleanser]
            [reagent.core :as r]
            [ajax.core :refer [GET]]
            [reagent-forms.core :refer [bind-fields init-field value-of]]))

(def app-state (r/atom []))
(def cur-view (r/atom {:viewable false}))

(defn json-handler [response]
  (let [parsed (js->clj response :keywordize-keys true)
        contents (:media parsed)
        contents-indexed (doall (map-indexed (fn [i itm] (merge itm {:idx i})) contents))]
    (apply swap! app-state conj contents-indexed))
     )    
  
(defn error-handler [{:keys [status status-text]}]
  (.log js/console "oops"))

(defn get-media-data []
  (GET "/av.txt" {:format :json
                  :response-format :json
                  :keywords? true
                  :handler json-handler
                  :error-handler error-handler
                  }))

(defn populate-viewer [elt]
  (let [cur-type (:type elt)
        urls (:url elt)]
    (when (> (count urls) 0) (swap! cur-view merge {:viewable true}))
    (for [url urls]
      (cleanser/parse-player cur-type url))
    )
  )
    
    


(defn media-viewer [idx]
  (let [viewable (:viewable @cur-view)
        elt (get @app-state idx)]
    (fn []
      (if viewable
        [:div (populate-viewer elt)]
        [:div "no view selected"]
      )
      ))
    )

(defn drop-down-menu []
     (get-media-data)
     (fn []
       [:div
       [:label "select option to view "]
       [:select {:on-change #(media-viewer (-> % .-target .-value))}
     (for [elt @app-state] ;;^{:key (:title elt) :value (:idx elt)}
         [:option {:key (:title elt) :value (:idx elt)} (str (:title elt))]
         )
        ]]))

(defn page []
  [:div
  [drop-down-menu]
  [media-viewer]
   ]
  )

(defn render []
  (r/render-component [page] (.getElementById js/document "app")))
